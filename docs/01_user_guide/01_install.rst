============
Installation
============

Instructions for different methods of installing adop:

* `From PyPI`_ (recommended)
* `From source`_

From PyPI
=========

Prerequisites:

* Python 3.7 or newer

Open command line and and install using pip:

* Linux:

  .. code-block:: console

    $ python3 -m pip install --user getpodcast

* Windows:

  .. code-block:: doscon

    > py -3 -m pip install --user getpodcast

From source
===========

Prerequisites:

* Python 3.7 or newer
* Git

Checkout with git:

.. code-block:: console

  $ git clone https://gitlab.com/fholmer/getpodcast.git
  $ pip install --user .

You can also install main branch directly from gitlab if git is not available:

.. code-block:: console

  $ pip install https://gitlab.com/fholmer/getpodcast/-/archive/master/getpodcast-master.zip

Or a specific version:

.. code-block:: console

  $ pip install https://gitlab.com/fholmer/getpodcast/-/archive/1.0.9/getpodcast-1.0.9.zip

