==========
Getpodcast
==========

* PyPI: https://pypi.org/project/getpodcast/
* Documentation: https://fholmer.gitlab.io/getpodcast
* Source code: https://gitlab.com/fholmer/getpodcast
* License: MIT

Summary
=======

Simplify downloading podcasts with getpodcast

Features
========

* No database or caching backend. RSS items are instead compared to file name,
  file size and file timestamp

* Configuration and hooks are stored in a single python file, which is also
  executable

Installation
============

* Linux:

  .. code-block:: bash

    $ python3 -m pip install --user getpodcast

* Windows:

  .. code-block:: doscon

    > py -3 -m pip install --user getpodcast

Usage
=====

Create a new file ``mypodcasts.py``:

.. code-block:: python

    #! /usr/bin/env python3

    import getpodcast

    opt = getpodcast.options(
        date_from='2021-01-01',
        root_dir='./podcast')

    podcasts = {
        "SGU": "https://feed.theskepticsguide.org/feed/sgu"
    }

    getpodcast.getpodcast(podcasts, opt)


Download podcasts:

.. code-block:: bash

    python3 mypodcasts.py --run


More help:

.. code-block:: bash

    python3 mypodcasts.py --help

Setup cronjob to download once a day:

.. code-block:: bash

    $ crontab -e

    0 19 * * * /usr/bin/python3 /home/myuser/mypodcasts.py --quiet --onlynew --run
